require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'
require "byebug"
class Game
  def initialize(p1, p2)
    @player_one = p1
    @player_two = p2
    @board = Board.new
    @current_player = @player_one
  end

  attr_reader :player_one, :player_two
  attr_accessor :current_player, :board

  def switch_players!
    if @current_player == @player_one
      @current_player = @player_two
    else
      @current_player = @player_one
    end
  end

  def play_turn
    # debugger
    @current_player.display(@board)
    move = @current_player.get_move
    until self.check_move(move)
      move = @current_player.get_move
    end
    @board.place_mark(move, @current_player.mark)
    self.switch_players!
  end

  def check_move(move)
    move.all? { |el| el <= 2 } && self.board[move].nil?
  end

  def self.play(p1, p2)
    puts "Press ENTER to begin the game"
    gets.chomp
    playthru = Game.new(p1, p2)
    until playthru.board.over?
      playthru.play_turn
    end
    # Two because I don't now which will show the board in terminal
    playthru.player_one.display(playthru.board)
    playthru.player_two.display(playthru.board)
    victor = playthru.board.winner
    if victor.nil?
      puts "It was a tie! Thanks for playing!"
    else
      puts "#{victor} won! Thanks for playing!"
    end
  end

end

# necessary info to get upon game start

def get_player_names
  names = { human: "", comp: "" }
  puts "What is your name?"
  names[:human] = gets.chomp
  puts "What is your opponent's name?"
  names[:comp] = gets.chomp
  names
end

def get_player_marks
  marks = { human: "", comp: "" }
  loop do
    puts "Would you like to play X or O?"
    player_mark = gets.chomp.upcase
    if player_mark == "X" || player_mark == "O"
      marks[:human] = player_mark.to_sym
      break
    end
    puts "Please choose X or O"
  end
  if marks[:human] == :X
    marks[:comp] = :O
  else
    marks[:comp] = :X
  end
  marks
end

def first_move
  puts "I will now flip a coin to see if you will go first or second"
  p_num = rand(1..2)
  if p_num == 1
    puts "You will be Player One and go first!"
  else
    puts "You will be Player Two and go second!"
  end
  p_num
end

names_hsh = get_player_names
marks_hsh = get_player_marks
human_num = first_move

human = HumanPlayer.new(names_hsh[:human])
human.mark = marks_hsh[:human]
comp = ComputerPlayer.new(names_hsh[:comp])
comp.mark = marks_hsh[:comp]

play_game = true
while play_game
  if human_num == 1
    Game.play(human, comp)
  else
    Game.play(comp, human)
  end
  puts "Would you like to play again (y/n)?"
  response = gets.chomp
  if response.downcase == "N"
    puts "Goodbye!"
    play_game = false
  end
end
