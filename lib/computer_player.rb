class ComputerPlayer
  def initialize(name)
    @name = name
  end

  attr_reader :name, :board
  attr_accessor :mark

  def display(board_obj)
    @board = board_obj
  end

  def get_move
    win = winning_move
    if win.nil?
      return [rand(0..2), rand(0..2)]
    else
      return win
    end
  end

  def winning_move
    @board.grid.each_with_index do |subarr, row|
      next if subarr.count(nil) > 1
      if (subarr - [nil]).all? { |el| el == @mark }
        return [row, subarr.index(nil)]
      end
    end
    @board.cols.each_with_index do |subarr, col|
      next if subarr.count(nil) > 1
      if (subarr - [nil]).all? { |el| el == @mark }
        return [subarr.index(nil), col]
      end
    end
    @board.diags.each_with_index do |subarr, d|
      next if subarr.count(nil) > 1
      if (subarr - [nil]).all? { |el| el == @mark }
        return [d, subarr.index(nil)]
      end
    end
    nil
  end

end
