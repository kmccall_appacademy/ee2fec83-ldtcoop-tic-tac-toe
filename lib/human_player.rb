class HumanPlayer
  def initialize(name)
    @name = name
  end

  attr_reader :name
  attr_accessor :mark

  def display(board)
    board.grid.each_with_index do |subarr, ind|
      row_string_arr = subarr.map do |el|
        if el.nil?
          " "
        else
          el.to_s
        end
      end
      puts " #{row_string_arr.join(' | ')} "
      puts "-----------" if ind < 2
    end
  end

  def get_move
    puts "Where would you like to put your mark? (row, col)"
    gets.chomp.split(",").map(&:to_i)
  end
end
