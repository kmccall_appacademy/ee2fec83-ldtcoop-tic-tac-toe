class Board
  def initialize(grid = Array.new(3) { Array.new(3) })
    @grid = grid
  end

  #lets us see the whole grid at once
  attr_reader :grid

  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    @grid[row][col] = mark
  end

  def place_mark(pos, mark)
    self[pos] = mark
  end

  def empty?(pos)
    self[pos].nil?
  end

  def winner
    @grid.each do |subarr|
      return subarr[0] if subarr.all? { |el| !el.nil? && el == subarr[0] }
    end
    self.cols.each do |subarr|
      return subarr[0] if subarr.all? { |el| !el.nil? && el == subarr[0] }
    end
    self.diags.each do |subarr|
      return subarr[0] if subarr.all? { |el| !el.nil? && el == subarr[0] }
    end
    nil
  end

  def over?
    # board is empty
    if @grid == [[nil, nil, nil], [nil, nil, nil], [nil, nil, nil]]
      false
    # someone wins
    elsif self.won?
      true
    # tie game
    elsif self.tie?
      true
    # ongoing game
    else
      false
    end
  end

  def won?
    if self.win_on_row?
      true
    elsif self.win_on_col?
      true
    elsif self.win_on_diag?
      true
    else
      false
    end
  end

  def win_on_row?
    @grid.each do |row|
      return true if row.all? do |el|
        !el.nil? && el == row[0]
      end
    end
    false
  end

  def win_on_col?
    self.cols.each do |col|
      return true if col.all? do |el|
        !el.nil? && el == col[0]
      end
    end
    false
  end

  def win_on_diag?
    self.diags.each do |diag|
      return true if diag.all? do |el|
        !el.nil? && el == diag[0]
      end
    end
    false
  end

  def cols
    @grid.transpose
  end

  def diags
    [
      [self[[0, 0]], self[[1, 1]], self[[2, 2]]],
      [self[[0, 2]], self[[1, 1]], self[[2, 0]]]
    ]
  end

  def tie?
    @grid.all? do |row|
      row.none?(&:nil?)
    end
  end
end
